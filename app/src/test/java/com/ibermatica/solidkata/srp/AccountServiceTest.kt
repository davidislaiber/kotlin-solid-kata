package com.ibermatica.solidkata.srp

import com.ibermatica.solidkata.srp.domain.Transaction
import com.ibermatica.solidkata.srp.repository.TransactionRepository
import com.ibermatica.solidkata.srp.repository.impl.TransactionRepositoryImpl
import com.ibermatica.solidkata.srp.service.Console
import org.junit.Before
import org.junit.Test

import com.ibermatica.solidkata.srp.service.AccountService
import io.mockk.*
import org.junit.After
import org.junit.Assert.assertEquals

class AccountServiceTest {

    private lateinit var transactionRepository : TransactionRepository
    private lateinit var console : Console

    private lateinit var sut : AccountService

    private lateinit var listUser1 : List<Transaction>
    private lateinit var listUser2 : List<Transaction>

    @Before
    fun setUp() {

        transactionRepository = mockk()
        console = spyk(Console())

        listUser1 = listOf( Transaction( user = 1, desc = "Art1", value = 20f ),
                            Transaction( user = 1, desc = "Art2", value = 15f ),
                            Transaction( user = 1, desc = "Art3", value = 5f ) )

        listUser2 = listOf( Transaction( user = 2, desc = "Art4", value = 20f ),
                            Transaction( user = 2, desc = "Art5", value = 15f ),
                            Transaction( user = 2, desc = "Art6", value = -5f ) )
    }

    @After
    fun cleanUp() {
        unmockkAll()
    }

    @Test
    fun `deposit amount into the account`() {

        val transactionRepository = spyk( TransactionRepositoryImpl() )

        sut = AccountService( transactionRepository, console )

        sut.deposit( 1, "deposit", 20f )

        verify { transactionRepository.add( Transaction(1, "deposit", 20f )) }
    }

    @Test
    fun `withdraw amount into the account`() {

        val transactionRepository = spyk( TransactionRepositoryImpl() )

        sut = AccountService( transactionRepository, console )

        sut.withdraw( 1, "withdraw", 30f )

        verify { transactionRepository.add( Transaction(1, "withdraw", -30f )) }
    }

    @Test
    fun `test report with user 2`() {

        every { transactionRepository.getTransactions() } returns listUser2

        sut = AccountService( transactionRepository, console )

        sut.printReport()

        verifySequence {
            console.println( "User    | Desc      | Value" )
            console.println( "--------|-----------|------" )
            console.println( "      2 |      Art4 | 20,00" )
            console.println( "      2 |      Art5 | 15,00" )
            console.println( "      2 |      Art6 | -5,00" )
        }
    }

    @Test
    fun `format user`() {
        sut = AccountService( transactionRepository, console )
        assertEquals( "      2", sut.formatUser(2) )
    }

    @Test
    fun `format short desc`() {
        sut = AccountService( transactionRepository, console )
        assertEquals( "     Art4", sut.formatDesc("Art4"))
    }

    @Test
    fun `format long desc`() {
        sut = AccountService( transactionRepository, console )
        assertEquals( "ThisIsALo", sut.formatDesc("ThisIsALongDescription"))
    }

    @Test
    fun `format value`() {
        sut = AccountService( transactionRepository, console )
        assertEquals( "-30,50", sut.formatValue(-30.5f))
    }



}