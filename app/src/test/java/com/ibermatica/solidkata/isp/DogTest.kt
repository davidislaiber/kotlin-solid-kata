package com.ibermatica.solidkata.isp

import com.ibermatica.solidkata.isp.domain.Dog
import com.ibermatica.solidkata.isp.service.Console
import io.mockk.spyk
import io.mockk.verify
import org.junit.Test

class DogTest {

    val console = Console()

    @Test
    fun `test dog running`() {

        var consoleSpy = spyk(console)

        val sut = Dog(consoleSpy)

        sut.run()

        verify { consoleSpy.println("Dog is running!!") }
    }

    @Test
    fun `test dog barking`() {

        var consoleSpy = spyk(console)

        val sut = Dog(consoleSpy)

        sut.bark()

        verify { consoleSpy.println("Wof!!") }
    }


}