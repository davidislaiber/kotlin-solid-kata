package com.ibermatica.solidkata.isp

import com.ibermatica.solidkata.isp.domain.Bird
import com.ibermatica.solidkata.isp.service.Console
import io.mockk.spyk
import io.mockk.verify
import org.junit.Test

class BirdTest {

    val console = Console()

    @Test
    fun `test bird running`() {

        var consoleSpy = spyk(console)

        val sut = Bird(consoleSpy)

        sut.run()

        verify { consoleSpy.println("Bird is running!!") }
    }

    @Test
    fun `test bird flying`() {

        var consoleSpy = spyk(console)

        val sut = Bird(consoleSpy)

        sut.fly()

        verify { consoleSpy.println("Bird is flying!!") }
    }


}