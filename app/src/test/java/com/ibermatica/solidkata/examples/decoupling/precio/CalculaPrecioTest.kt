package com.ibermatica.solidkata.examples.decoupling.precio

import org.junit.Assert.assertEquals
import org.junit.Test

internal class CalculaPrecioTest {

    @Test
    fun calcular() {

        val sut = CalculaPrecio()

        val res = sut.calc( "ORD", "S", 2, 2 )

        assertEquals(400, res )

    }
}