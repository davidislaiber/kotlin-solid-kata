package com.ibermatica.solidkata.examples.decoupling.miniesa

import com.ibermatica.solidkata.examples.decoupling.miniesa.MiniESA
import org.junit.Assert.assertEquals
import org.junit.Test

class MiniESATest {

    private val sut = MiniESA()

    @Test( expected = Exception::class)
    fun `Venta de un cupón pasando un patrón con longitud incorrecta`() {
        sut.vender( "" )
    }

    @Test
    fun `Venta normal`() {

        val cupon = sut.vender( "**1**" )

        assertEquals( '1', cupon.numero[2] )

        println( cupon.toString() )
    }

}