package com.ibermatica.solidkata.examples.tennis

import TennisGame1
import org.junit.Assert
import org.junit.Test

class TennisTest {

    @Test
    fun `check Tennis1 0 vs 0 is Love-All`() {
        val game = TennisGame1( "player1", "player2" )
        val score = game.getScore()
        Assert.assertEquals( score, "Love-All" )
    }

    @Test
    fun `check Tennis1 1 vs 0 is Fifteen-Love`() {
        val game = TennisGame1( "player1", "player2" )
        game.wonPoint("player1")
        val score = game.getScore()
        Assert.assertEquals( score, "Fifteen-Love" )
    }

}