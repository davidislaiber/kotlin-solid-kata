package com.ibermatica.solidkata.examples.decoupling.miniesa

import com.ibermatica.solidkata.examples.decoupling.miniesa.Printer
import com.ibermatica.solidkata.examples.decoupling.miniesa.VentaCupon
import io.mockk.spyk
import io.mockk.verifySequence
import org.junit.Test

class VentaCuponTest {

    @Test
    fun prueba() {

        val printerSpy = spyk(Printer())

        val venta = VentaCupon( printerSpy )

        venta.vender( "**4**" )

        verifySequence {
            printerSpy.println( "------------------" )
            printerSpy.println( "|     EXTRA      |" )
            printerSpy.println( "| NUMERO : 52424 |" )
            printerSpy.println( "| SERIE : 099    |" )
            printerSpy.println( "| ID : 727170    |" )
            printerSpy.println( "------------------" )
        }

    }
}