package com.ibermatica.solidkata.examples.decoupling.miniesa

import com.ibermatica.solidkata.examples.decoupling.miniesa.MiniESA
import com.ibermatica.solidkata.examples.decoupling.miniesa.PagarCupon
import com.ibermatica.solidkata.examples.decoupling.miniesa.Printer
import io.mockk.spyk
import io.mockk.verifySequence
import org.junit.Test

class PagarCuponTest {

    @Test
    fun pruebaImpresionOk() {

        val printerSpy = spyk(Printer())

        val pago = PagarCupon( MiniESA(), printerSpy )

        pago.pagar( "1234" )

        verifySequence {
            printerSpy.println("------------------")
            printerSpy.println("|    PREMIO!!!   |")
            printerSpy.println("|                |")
            printerSpy.println("|       1,00 €   |")
            printerSpy.println("------------------")
        }

    }
}