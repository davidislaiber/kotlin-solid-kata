package com.ibermatica.solidkata.lsp

import com.ibermatica.solidkata.lsp.domain.ElectricCar
import com.ibermatica.solidkata.lsp.domain.FillingStation
import com.ibermatica.solidkata.lsp.domain.PetrolCar
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException

@Rule
var exception = ExpectedException.none()

class FillingStationTest {

    companion object {
        val FULL = 100
    }

    private val sut = FillingStation()

    @Test
    fun `refuel petrol car`() {
        val petrolCar = PetrolCar()

        sut.refuel( petrolCar )

        assertEquals( FULL, petrolCar.fuelTankLevel() )
    }

    @Test
    fun `check refuel on a electric car`() {
        val electricCar = ElectricCar()

        exception.expect(UnsupportedOperationException::class.java)
        exception.expectMessage("Test exception! Electric car can not be refueled")

        sut.refuel( electricCar )
    }

    @Test
    fun `charge electric car`() {
        val electricCar = ElectricCar()

        sut.charge( electricCar )

        assertEquals( FULL, electricCar.batteryLevel() )
    }

    @Test
    fun `check charge a petrol car`() {
        val petrolCar = PetrolCar()

        exception.expect(UnsupportedOperationException::class.java)
        exception.expectMessage("Test exception! A petrol car can not be charged")

        sut.refuel( petrolCar )
    }

}