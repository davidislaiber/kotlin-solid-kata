package com.ibermatica.solidkata.lsp

import com.ibermatica.solidkata.lsp.domain.ElectricCar
import com.ibermatica.solidkata.lsp.domain.PetrolCar
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class VehicleTest {

    @Test
    fun `arrancar motor a gasolina`() {
        val sut = PetrolCar()

        sut.startEngine()

        assertTrue( sut.engineIsStarted() )
    }

    @Test
    fun `arrancar motor electrico`() {
        val sut = ElectricCar()

        sut.startEngine()

        assertTrue( sut.engineIsStarted() )
    }

    @Test
    fun `parar motor a gasolina`() {
        val sut = PetrolCar()

        sut.startEngine()
        sut.stopEngine()

        assertFalse( sut.engineIsStarted() )
    }

    @Test
    fun `parar motor electrico`() {
        val sut = ElectricCar()

        sut.startEngine()
        sut.stopEngine()

        assertFalse( sut.engineIsStarted() )
    }

}