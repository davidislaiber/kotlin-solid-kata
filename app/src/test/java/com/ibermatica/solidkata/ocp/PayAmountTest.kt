package com.ibermatica.solidkata.ocp

import com.ibermatica.solidkata.ocp.domain.Employee
import com.ibermatica.solidkata.ocp.domain.EmployeeType
import org.junit.Assert.assertEquals
import org.junit.Test

class PayAmountTest {

    companion object {
        val SALARY = 100
        val BONUS = 1000
    }

    @Test
    fun `sumar bonus al manager`() {

        val sut = Employee( SALARY, BONUS, EmployeeType.MANAGER )

        assertEquals( 1100, sut.payAmount() )
    }

    @Test
    fun `no sumar bonus al ingeniero`() {

        val sut = Employee( SALARY, BONUS, EmployeeType.ENGINEER )

        assertEquals( 100, sut.payAmount() )
    }

}