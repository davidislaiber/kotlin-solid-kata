package com.ibermatica.solidkata.lsp.domain

import java.lang.UnsupportedOperationException

class PetrolCar : Vehicle() {

    companion object {
        val FUEL_TANK_FULL = 100
    }

    private var fuelTankLevel = 0

    override fun fillWithFuel() {
        fuelTankLevel = FUEL_TANK_FULL
    }

    override fun chargeBattery() {
        throw UnsupportedOperationException("A petrol car cannot be recharged")
    }

    fun fuelTankLevel() : Int {
        return fuelTankLevel
    }

}