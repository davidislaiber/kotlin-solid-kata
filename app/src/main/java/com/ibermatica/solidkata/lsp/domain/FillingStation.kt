package com.ibermatica.solidkata.lsp.domain

class FillingStation {

    fun refuel( vehicle : Vehicle ) {
        if( vehicle is PetrolCar ) {
            vehicle.fillWithFuel()
        }
    }

    fun charge( vehicle: Vehicle ) {
        if( vehicle is ElectricCar ) {
            vehicle.chargeBattery()
        }
    }
}