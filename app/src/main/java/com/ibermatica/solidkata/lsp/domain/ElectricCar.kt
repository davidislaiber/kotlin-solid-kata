package com.ibermatica.solidkata.lsp.domain

import java.lang.UnsupportedOperationException

class ElectricCar : Vehicle() {

    companion object {
        val BATTERY_FULL = 100
    }

    private var batteryLevel = 0;

    override fun fillWithFuel() {
        throw UnsupportedOperationException("It's an electric car!!!")
    }

    override fun chargeBattery() {
        batteryLevel = BATTERY_FULL
    }

    fun batteryLevel() : Int {
        return batteryLevel
    }

}