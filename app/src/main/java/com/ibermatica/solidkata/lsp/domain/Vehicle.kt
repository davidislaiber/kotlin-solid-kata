package com.ibermatica.solidkata.lsp.domain

abstract  class Vehicle {

    var engineStarted : Boolean = false

    fun startEngine() {
        engineStarted = true
    }

    fun stopEngine() {
        engineStarted = false
    }

    fun engineIsStarted() : Boolean {
        return engineStarted
    }

    abstract fun fillWithFuel()

    abstract fun chargeBattery()

}