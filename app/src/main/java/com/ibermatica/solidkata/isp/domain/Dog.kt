package com.ibermatica.solidkata.isp.domain

import com.ibermatica.solidkata.isp.service.Console
import java.lang.UnsupportedOperationException

class Dog(val console : Console) : Animal {

    override fun fly() {
        throw UnsupportedOperationException("Dogs can't fly!!")
    }

    override fun run() {
        console.println("Dog is running!!")
    }

    override fun bark() {
        console.println("Wof!!")
    }
}