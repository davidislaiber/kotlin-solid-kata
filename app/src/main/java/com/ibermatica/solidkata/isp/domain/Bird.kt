package com.ibermatica.solidkata.isp.domain

import com.ibermatica.solidkata.isp.service.Console
import java.lang.UnsupportedOperationException

class Bird(var console : Console) : Animal {

    override fun fly() {
        console.println("Bird is flying!!")
    }

    override fun run() {
        console.println("Bird is running!!")
    }

    override fun bark() {
        throw UnsupportedOperationException("Birds can't bark!!")
    }
}