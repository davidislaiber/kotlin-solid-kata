package com.ibermatica.solidkata.isp.domain

interface Animal {

    fun fly()
    fun run()
    fun bark()

}