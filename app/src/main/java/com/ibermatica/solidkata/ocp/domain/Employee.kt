package com.ibermatica.solidkata.ocp.domain

class Employee( val salary : Int, val bonus : Int, val type : EmployeeType ) {

    fun payAmount() : Int {

        var amount = when( type ) {
            EmployeeType.MANAGER -> salary + bonus
            EmployeeType.ENGINEER ->  salary
            else ->  0
        }

        return amount
    }


}