package com.ibermatica.solidkata.ocp.domain;

public enum EmployeeType {
    MANAGER,
    ENGINEER
}
