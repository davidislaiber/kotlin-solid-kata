package com.ibermatica.solidkata.srp.repository.impl

import com.ibermatica.solidkata.srp.domain.Transaction
import com.ibermatica.solidkata.srp.repository.TransactionRepository

class TransactionRepositoryImpl : TransactionRepository() {

    var listTransactions : MutableList<Transaction> = mutableListOf()

    override fun add(transaction: Transaction) {
        listTransactions.add( transaction )
    }

    override fun getTransactions(): List<Transaction> {
        return listTransactions
    }


}