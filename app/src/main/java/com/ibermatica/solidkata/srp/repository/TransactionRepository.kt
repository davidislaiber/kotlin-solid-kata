package com.ibermatica.solidkata.srp.repository

import com.ibermatica.solidkata.srp.domain.Transaction

abstract class TransactionRepository {

    abstract fun add(transaction: Transaction )
    abstract fun getTransactions() : List<Transaction>

}