package com.ibermatica.solidkata.srp.domain

data class Transaction (

    var user: Int,     // Código de usuario
    var desc: String,  // Descripción
    var value: Float     // Importe
)
