package com.ibermatica.solidkata.srp.service

import com.ibermatica.solidkata.srp.domain.Transaction
import com.ibermatica.solidkata.srp.repository.TransactionRepository

class AccountService(var transactions: TransactionRepository,
                     var console: Console)
{

    companion object {

        private val HEADER        = "User    | Desc      | Value"
        private val ROW_SEPARATOR = "--------|-----------|------"
        private val SEPARATOR = "|"

    }

    fun deposit( user : Int, desc : String, value : Float ) {
        transactions.add( Transaction( user, desc, value) )
    }

    fun withdraw( user : Int, desc : String, value : Float ) {
        transactions.add( Transaction( user, desc, -value) )
    }

    fun printReport() {
        printHeaders()
        printTransactions()
    }

    private fun printHeaders() {
        console.println(HEADER)
        console.println(ROW_SEPARATOR)
    }

    private fun printTransactions() {
        transactions.getTransactions().forEach{
            console.println( "${formatUser(it.user)} ${SEPARATOR} ${formatDesc(it.desc)} ${SEPARATOR} ${formatValue(it.value)}")
        }
    }

    fun formatUser( user : Int ) : String {
        return "%7d".format(user)
    }

    fun formatDesc( desc : String ) : String {
        return "%9s".format(desc.take(9) )
    }

    fun formatValue( value : Float ) : String {
        return "%4.2f".format(value)
    }

}