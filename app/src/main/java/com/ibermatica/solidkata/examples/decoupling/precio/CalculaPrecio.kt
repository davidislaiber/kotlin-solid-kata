package com.ibermatica.solidkata.examples.decoupling.precio

class CalculaPrecio { // No toques esta línea

    fun calc( tip : String, add : String, sor : Int, apu : Int ) : Int { // No toques esta línea

        var precio = 0

        if( tip == "ORD" ) {
            val pUnoORD = DAOPrecio.ConsultaPrecioDeProductoEnEVO( tip )
            precio = pUnoORD * sor * apu
            if( add == "S" ) {
                val pUnoAOR = DAOPrecio.ConsultaPrecioDeProductoEnEVO( "AOR" )
                val ptemp = pUnoAOR * sor * apu
                precio = precio + ptemp
            }
        }
        else if( tip == "VIE" ) {
            val pUnoVIE = DAOPrecio.ConsultaPrecioDeProductoEnEVO( tip )
            precio = pUnoVIE * sor * apu
            if( add == "S" ) {
                val pUnoXXL = DAOPrecio.ConsultaPrecioDeProductoEnEVO( "XXL" )
                val ptemp = pUnoXXL * sor * apu
                precio = precio + ptemp
            }
        }
        else if( tip == "DOM" ) {
            val pUnoVIE = DAOPrecio.ConsultaPrecioDeProductoEnEVO( tip )
            precio = pUnoVIE * sor * apu
        }
        else if( tip == "EXT" ) {
            val pUnoVIE = DAOPrecio.ConsultaPrecioDeProductoEnEVO( tip )
            precio = pUnoVIE * sor * apu
        }

        return precio
    }

}

