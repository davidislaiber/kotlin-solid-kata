package com.ibermatica.solidkata.examples.decoupling.miniesa

class PagarCupon(val miniESA : IMiniEsa, val printer : IPrinter){

    fun pagar( codbar : String ) {

        val res = miniESA.pagar( codbar )

        if( res.status == "OK" ) {
            printer.println("------------------")
            printer.println("|    PREMIO!!!   |")
            printer.println("|                |")
            printer.println("|       %04.2f €   |".format(res.prize/100f))
            printer.println("------------------")
        }

    }
}