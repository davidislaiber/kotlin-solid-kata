package com.ibermatica.solidkata.examples.decoupling.miniesa

import kotlin.random.Random

interface IMiniEsa {
    fun vender( pattern : String ) : CuponEntity
    fun pagar( codbar : String ) : PagoEntity
}

class MiniESA : IMiniEsa {

    override fun vender( pattern : String ) : CuponEntity {

        if ( pattern.length != 5 ) throw Exception("El pattern debe tener una longitud de 5 caracteres")

        var number = ""
        var serie = 0
        var id = 0

        pattern.forEach {
            number += if( it == '*' ) Random.nextInt(0, 9).toString() else it
        }

        serie = Random.nextInt(120)
        id = Random.nextInt( 999999 )

        return CuponEntity( number, "%03d".format(serie), "%06d".format(id) )
    }

    override fun pagar( codbar : String ) : PagoEntity {

        val odds = Random.nextInt(2)

        if( odds == 0 ) {
            return PagoEntity( "ERROR", 0, "" )
        } else {
            return PagoEntity( "OK", 100, "%06d".format(Random.nextInt( 999999 )) )
        }
    }
}