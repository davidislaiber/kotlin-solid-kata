package com.ibermatica.solidkata.examples.decoupling.miniesa

data class CuponEntity (
    var numero: String,
    var serie: String,
    var id: String
)
