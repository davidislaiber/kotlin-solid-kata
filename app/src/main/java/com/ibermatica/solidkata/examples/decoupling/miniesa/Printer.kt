package com.ibermatica.solidkata.examples.decoupling.miniesa

interface IPrinter {
    fun println( log : String )
}

class Printer : IPrinter {

    override fun println( log : String ) {
        kotlin.io.println(log)
    }

}