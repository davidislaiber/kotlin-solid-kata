package com.ibermatica.solidkata.examples.decoupling.miniesa

import java.util.*

class VentaCupon( val printer : Printer) {

    fun vender(patron: String) : Int {

        val esa = MiniESA()

        try {
            val cupon = esa.vender(patron)
            imprimeCupon(cupon)
        }
        catch (e: Exception) {
            return -1
        }

        return 0
    }

    private fun imprimeCupon(cupon: CuponEntity) {

        val calendar: Calendar = Calendar.getInstance()
        val day: Int = calendar.get(Calendar.DAY_OF_WEEK)

        printer.println("------------------")

        // Lunes y Viernes el premio es EXTRA!!
        when (day) {
            Calendar.MONDAY -> { printer.println("|     EXTRA      |") }
            Calendar.FRIDAY -> { printer.println("|     EXTRA      |") }
        }

        printer.println("| NUMERO : " + cupon.numero + " |")
        printer.println("| SERIE : " + cupon.serie + "    |")
        printer.println("| ID : " + cupon.id + "    |")
        printer.println("------------------")
    }

}