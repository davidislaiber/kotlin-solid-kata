package com.ibermatica.solidkata.examples.decoupling.miniesa

data class PagoEntity (
    var status : String,
    var prize : Int,
    var payId : String
)
